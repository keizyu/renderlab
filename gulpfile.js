let gulp = require('gulp');
let gutil = require('gulp-util');
let sass = require('gulp-sass');
let browserSync = require('browser-sync');
let autoprefixer = require('gulp-autoprefixer');
let babel = require('gulp-babel');
// let uglify = require('gulp-uglify');
let uglify = require('gulp-uglifyes');
let concat = require('gulp-concat');
let jshint = require('gulp-jshint');
let header  = require('gulp-header');
let rename = require('gulp-rename');
let cssnano = require('gulp-cssnano');
let sourcemaps = require('gulp-sourcemaps');
let package = require('./package.json');


let banner = [
  '/*!\n' +
  ' * Last Build ' + new Date() + '\n' +
  ' * <%= package.name %>\n' +
  ' * <%= package.title %>\n' +
  ' * <%= package.url %>\n' +
  ' * @author <%= package.author %>\n' +
  ' * @version <%= package.version %>\n' +
  ' * Copyright ' + new Date().getFullYear() + '. <%= package.license %> licensed.\n' +
  ' */',
  '\n'
].join('');

gulp.task('css', function () {
    return gulp.src('src/scss/style.scss')
    .pipe(sourcemaps.init())
    .pipe(sass().on('error', sass.logError))
    .pipe(autoprefixer('last 4 version'))
    .pipe(gulp.dest('app/assets/css'))
    .pipe(cssnano())
    .pipe(rename({ suffix: '.min' }))
    .pipe(header(banner, { package : package }))
    .pipe(sourcemaps.write())
    .pipe(gulp.dest('app/assets/css'))
    .pipe(browserSync.reload({stream:true}));
});

gulp.task('js',function(){
  gulp.src([
      'src/js/components/jquery/jquery.min.js',
      'src/js/vendor/*.js',
      'src/js/scripts.js'
  ])
    .pipe(sourcemaps.init())
    .pipe(jshint('.jshintrc'))
    .pipe(jshint.reporter('default'))
    .pipe(header(banner, { package : package }))
    .pipe(concat('bundle.js'))
    .pipe(gulp.dest('app/assets/js'))
    .pipe(uglify({
        mangle: false,
        ecma: 6
    }))
    .on('error', function (err) { gutil.log(gutil.colors.red('[Error]'), err.toString()); })
    .pipe(header(banner, { package : package }))
    .pipe(rename({ suffix: '.min' }))
    .pipe(sourcemaps.write())
    .pipe(gulp.dest('app/assets/js'))
    .pipe(browserSync.reload({stream:true, once: true}));
});

gulp.task('browser-sync', function() {
    browserSync.init(null, {
        server: {
            baseDir: "app"
        }
    });
});

gulp.task('bs-reload', function () {
    browserSync.reload();
});

gulp.task('app', ['css', 'js', 'browser-sync'], function () {
    gulp.watch("src/scss/**/*.scss", ['css']);
    gulp.watch("src/js/*.js", ['js']);
    gulp.watch("app/*.html", ['bs-reload']);
});
