(function ($, window, document, undefined) {

  'use strict';

  $(function () {

      if($.browser.mobile) {
        $("#body").addClass("is-mobile")
        // Remove videos on mobile
        $('#bodyBg, #crewVideo-desktop, #workVideo, #contactoBg, #phone-desktop, #whats-desktop').remove();

        // Add Crew mobile video
        $('#crewNav').click(function(){
          $('#crewVideo-mobile')[0].play();
        });

        // Tools logic
        $("#tools")
            .on('touchstart', function () {
                $(this).data('moved', '0');
            })
            .on('touchmove', function () {
                $(this).data('moved', '1');
            })
            .on('touchend', function (e) {
                let clientX = e.originalEvent.changedTouches[0].clientX,
                    clientY = e.originalEvent.changedTouches[0].clientY;

                let imagesArr = [];

                for (let i = 0; i < 62; i++) {
                  let imgSrc = 'assets/img/tools-mobile/ctrl' + [i+1] + '.jpg';
                  imagesArr.push(imgSrc);
                }

                let rand = imagesArr[Math.floor(Math.random() * imagesArr.length)];
                let ap = $(`<img src='${rand}' style='left:${(clientX - 115) + 'px'}; top:${(clientY - 75) + 'px'}' />`);

                // Custom Cursor for Tools

                let follower, init, positionElement, printout, timer;
                follower = document.getElementById('follower');
                printout = document.getElementById('printout');

                positionElement = event => {
                    let mouse;
                    mouse = {
                      x: clientX,
                      y: clientY
                    };

                    follower.style.top = mouse.y + 'px';
                    return follower.style.left = mouse.x + 'px';
                };

                timer = false;

                if($(this).data('moved') == 0){

                    $('#gallery').append(ap);

                    // Custom Cursor for Tools
                    let _event;
                    _event = e;
                    timer = setTimeout(() => {
                      return positionElement(_event);
                  }, 100);
                }


            });

      } else {
          console.log('is desktop');
          $('#crewVideo-mobile').remove();
          $('#phone-mobile').remove();
          $('#whats-mobile').remove();

          // Play Videos
          $('#crewNav').click(function(){ $('#crewVideo-desktop')[0].play(); });
          $('#workNav').on('click', function(){ $('#workVideo')[0].play(); });
          $('#contactoNav').on('click', function(){ $('#contactoBg')[0].play(); });

          /*
          Show/hide background elements on nav mouse hover
          ********************************/

          // CREW
          $('#crewNav').mouseenter(function(){ $('#bodyBg-crew').removeClass('hide'); });
          $('#crewNav').mouseleave(function(){ $('#bodyBg-crew').addClass('hide'); });

          // TOOLS
          $('#toolsNav').mouseenter(function(){ $('#bodyBg-tools').removeClass('hide'); });
          $('#toolsNav').mouseleave(function(){ $('#bodyBg-tools').addClass('hide'); });

          // PLAY
          $('#playNav').mouseenter(function(){ $('#bodyBg-play').removeClass('hide'); });
          $('#playNav').mouseleave(function(){ $('#bodyBg-play').addClass('hide'); });

          // WORK
          $('#workNav').mouseenter(function(){ $('#bodyBg-work').removeClass('hide'); });
          $('#workNav').mouseleave(function(){ $('#bodyBg-work').addClass('hide'); });

          // Tools Logic

          let oldMath = 0;

          $('#tools').on('mousemove', function(e){
              let imagesArr = [];
              let startingTop = 0;
              let startingLeft = 0;
              let math = Math.round(Math.sqrt(Math.pow(startingTop - e.clientY, 2) +Math.pow(startingLeft - e.clientX, 2))) + 'px';

              for (let i = 0; i < 61; i++) {
                var imgSrc = 'assets/img/tools/ctrl' + [i+1] + '.jpg';
                imagesArr.push(imgSrc);
              }

              let rand = imagesArr[Math.floor(Math.random() * imagesArr.length)];
              let ap = $(`<img src='${rand}' style='left:${(e.clientX / 1.5) + 'px'}; top:${(e.clientY / 1.5) + 'px'}' />`);

              if(Math.abs(parseInt(math) - oldMath) > 40){
                  //you have moved 40 pixles, then append image
                  $('#gallery').append(ap);
                  oldMath = parseInt(math);
              }

          });

          // Custom Cursor for Tools

            let follower, init, mouseX, mouseY, positionElement, printout, timer;
            follower = document.getElementById('follower');
            printout = document.getElementById('printout');

            mouseX = event => {
              return event.clientX;
            };

            mouseY = event => {
              return event.clientY;
            };

            positionElement = event => {
              var mouse;
              mouse = {
                x: mouseX(event),
                y: mouseY(event) };

              follower.style.top = mouse.y + 'px';
              return follower.style.left = mouse.x + 'px';
            };

            timer = false;

            window.onmousemove = init = event => {
              var _event;
              _event = event;
              return timer = setTimeout(() => {
                return positionElement(_event);
              }, 1);
            };

      }

    // Insert Audio Tag

    $('[data-audio-url]').each(function(){
    	$(this).on('click', function() {
    		$(this).css('opacity', '1');
    		let mp3Url = $(this).attr('data-audio-url');
    		let a = new Audio(mp3Url);
    		$('#audio').html(a);
    		stopAllAudio();
    		a.play();
    		playWave.classList.add('animated-wave');

            let myAudio = document.querySelector('#audio audio');
            myAudio.addEventListener("ended", function(){
                 myAudio.currentTime = 0;
                 playWave.classList.remove('animated-wave');
            });
    	});
    });

    // Stop all Audio

    $('#wave').on('click', function(){
    	stopAllAudio();
    	$('[data-audio-url]').each(function(){
    		$(this).css('opacity', '.3');
    	});
    });

    function stopAllAudio(){
        $('audio').each(function(){
            this.pause();
            this.currentTime = 0;
        });
        playWave.classList.remove('animated-wave');
    }


    // Pointer

    $('button.btn--back').mouseenter(function(){
        $('#follower').addClass('no-cursor');
    });

    $('button.btn--back').mouseleave(function(){
        $('#follower').removeClass('no-cursor')
    });

    // element to detect scroll direction
    let el = $("#play .box"),
    mousey = document.querySelector('#mousey'),
    scrollTimer = null;

    el.scroll(function () {
        if (scrollTimer) {
            clearTimeout(scrollTimer);   // clear any previous pending timer
        }
        scrollTimer = setTimeout(handleScroll, 10000);   // set new timer
    });

    function handleScroll() {
        scrollTimer = null;
        anime({
            targets: mousey,
            duration: 100,
            easing: 'easeOutExpo',
            opacity: {
                value: 1,
                duration: 300,
                easing: 'linear'
            }
        });
    }

    // hide mouse icon on scroll
    // initialize last scroll position
    let lastY = el.scrollTop();

    el.on('scroll', function() {
        // get current scroll position
        let currY = el.scrollTop(),

        // determine current scroll direction
        y = (currY > lastY) ? 'down' : ((currY === lastY) ? 'none' : 'up');

        // hide mousey when scroll
        if(y == 'down' || 'up'){
            anime.remove(mousey);
    		anime({
    			targets: mousey,
    			duration: 400,
    			easing: 'easeOutExpo',
    			opacity: {
    				value: 0,
    				duration: 400,
    				easing: 'linear'
    			}
    		});
        }

        // update last scroll position to current position
        lastY = currY;

    });


    // Nav Logic

    // The content items and the back control.
    let navItems = [].slice.call(document.querySelectorAll('.tabsnav > .tabsnav__item'));
    let contentItems = [].slice.call(document.querySelectorAll('.tabscontent > .tabscontent__item')),
    backCtrl = document.querySelector('.tabscontent > button.btn--back'),
    // menu ctrl for smaller screens (the tabs are not initially shown and toggling this button will show/hide the tabs)
    menuCtrl = document.querySelector('button.btn--menu'),
    logo = document.querySelector('#logo'),
    loader = document.querySelector('#loader'),

    showContact = document.querySelector('.contacto'),
    playWave = document.querySelector('#wave');

    let tnav = new TabsNav(document.querySelector('nav.tabsnav'), {
    		movable: 'all',
    		layout: 'horizontal',
    		animeduration: 600,
    		animeeasing: [0.2,1,0.3,1],
    		animedelay: 50,
    		onOpenBarsUpdate: openTabCallback,
    		onOpenTab: function() {
    			// Show the back button after the tab is open.
    			anime({
    				targets: backCtrl,
    				duration: 600,
    				easing: 'easeOutExpo',
    				scale: [0,1],
    				opacity: {
    					value: 1,
    					duration: 300,
    					easing: 'linear'
    				}
    			});
                anime({
                    targets: playWave,
                    duration: 1200,
                    easing: 'easeOutExpo',
                    scale: [0,1],
                    opacity: {
                        value: 1,
                        duration: 300,
                        easing: 'linear'
                    }
                });
                anime({
                    targets: mousey,
                    duration: 1600,
                    easing: 'easeOutExpo',
                    scale: [0,1],
                    opacity: {
                        value: 1,
                        duration: 300,
                        easing: 'linear'
                    }
                });
    		}
    	}),

	isContentShown = false, current;

	window.onload = function(){
		menuCtrl.classList.add('btn--menu-active');
        logo.classList.add('loaded');
        loader.classList.add('none');
        document.querySelector('#contactoNavBtn').classList.add('loaded');

		var state = tnav.toggleVisibility();
		if( state === 0 ) {
			menuCtrl.classList.remove('btn--menu-active');

			// Scale up content
			anime.remove('.content');
			anime({
				targets: '.content',
				duration: 100,
				easing: [0.2,1,0.7,1],
				opacity: 1,
				scale: 1
			});
		}
	};

    function openTabCallback(anim, idx, tab) {
    	if( anim.progress > 40 && !isContentShown ) {
    		isContentShown = true;
    		current = idx;

    		let contentItem = contentItems[idx],
    			content = contentItem.querySelector('.box');

            let navItem = navItems[idx];
            navItem.classList.add('tabsnav__item--current');

    		// Hide the content elements.
    		content.style.opacity = 0;
    		// Show content item.
    		contentItem.style.opacity = 1;
    		contentItem.classList.add('tabscontent__item--current');

            //NAV width fix
            if($.browser.mobile || window.innerWidth < 600){
                $( '.tabsnav__bar' ).css('opacity','1');
            } else {
                $( '.tabsnav__bar' ).css({ 'width':'100%', 'opacity': '1'});
                $( '.tabsnav__item' ).css('width', '100%');
            }

            $( 'nav.tabsnav' ).addClass('.tabsnav--active');

    		// Animate content elements in.
    		// anime.remove(content);
    		anime({
    			targets: content,
    			easing: [0.2,1,0.3,1],
    			duration: 600,
    			translateY: [400,0],
    			opacity: {
    				value: 1,
    				duration: 600,
    				easing: 'linear'
    			}
    		});
    	}
    }

    backCtrl.addEventListener('click', closeTabs);

    function closeTabs() {
    	if( !tnav.isOpen ) return;

    	var contentItem = contentItems[current],
    		content = contentItem.querySelector('.box');

    	// Hide the content elements.
    	anime.remove(content);
    	// Animate content elements out.
    	anime({
    		targets: content,
    		duration: 600,
    		easing: [0.2,1,0.3,1],
    		translateY: [0,400],
    		opacity: {
    			value: 0,
    			duration: 100,
    			easing: 'linear'
    		},
    		update: function(anim) {
    			if( anim.progress > 30 && isContentShown ) {
    				isContentShown = false;
    				// Close tab.
    				tnav.close();
    			}
    		},
    		complete: function() {
    			// Hide content item.
    			contentItem.style.opacity = 0;
    			contentItem.classList.remove('tabscontent__item--current');
    		}
    	});

    	// Hide back ctrl
    	anime.remove(backCtrl);
    	anime({
    		targets: backCtrl,
    		duration: 300,
    		easing: 'easeOutExpo',
    		scale: [1,0],
    		opacity: {
    			value: 0,
    			duration: 100,
    			easing: 'linear'
    		}
    	});

        anime.remove(playWave);
    	anime({
    		targets: playWave,
    		duration: 300,
    		easing: 'easeOutExpo',
    		scale: [1,0],
    		opacity: {
    			value: 0,
    			duration: 100,
    			easing: 'linear'
    		}
    	});

        anime.remove(mousey);
    	anime({
    		targets: mousey,
    		duration: 300,
    		easing: 'easeOutExpo',
    		scale: [1,0],
    		opacity: {
    			value: 0,
    			duration: 100,
    			easing: 'linear'
    		}
    	});



        //NAV width fix
        if($.browser.mobile || window.innerWidth < 600){
            $( '.tabsnav__bar' ).css('opacity','0');
        } else {
            $( '.tabsnav__bar' ).css({ 'width':'0', 'opacity': '0'});
            $( '.tabsnav__item' ).css('width', '36%');
        }

        $( 'nav.tabsnav' ).removeClass('.tabsnav--active');

        // Clear gallery data
    	if($('#tools').hasClass('tabscontent__item--current')){
    		setTimeout(function(){
    			$( '#gallery' ).html('');
    		}, 500);
    	}

    	if($('#crew').hasClass('tabscontent__item--current')){
    		$('#crewVideo-desktop, #crewVideo-mobile')[0].pause();
    		$('#crewVideo-desktop, #crewVideo-mobile')[0].currentTime = 0;
    	}

    	if($('#play').hasClass('tabscontent__item--current')){
    		stopAllAudio();
    	}
    }

    menuCtrl.addEventListener('click', toggleTabs);

    function toggleTabs() {
    	var state = tnav.toggleVisibility();
    	if( state === 0 ) {
    		menuCtrl.classList.remove('btn--menu-active');
    		showContact.classList.remove('hide');

    		// Scale up content
    		anime.remove('.content');
    		anime({
    			targets: '.content',
    			duration: 600,
    			easing: [0.2,1,0.7,1],
    			opacity: 1,
    			scale: 1
    		});
            if($.browser.mobile || window.innerWidth < 600){
                logo.classList.remove('loaded');
            }
    	}
    	else if( state === 1 ) {
    		menuCtrl.classList.add('btn--menu-active');
    		showContact.classList.add('hide');

    		// Scale down content
    		anime.remove('.content');
    		anime({
    			targets: '.content',
    			duration: 600,
    			easing: [0.2,1,0.7,1],
    			opacity: 0.2,
    			scale: 0.9
    		});

            if($.browser.mobile || window.innerWidth < 600){
                logo.classList.add('loaded');
            }
    	}
    }

  });


})(jQuery, window, document);
